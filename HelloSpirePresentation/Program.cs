﻿using System;
using System.Drawing;
using Spire.Presentation;
using Spire.Presentation.Drawing;


namespace HelloSpirePresentation
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a PPT document
            Presentation presentation = new Presentation();

            //Add a new shape to the PPT document
            RectangleF rec = new RectangleF(presentation.SlideSize.Size.Width / 2 - 250, 80, 500, 150);
            IAutoShape shape = presentation.Slides[0].Shapes.AppendShape(ShapeType.Rectangle, rec);

            shape.ShapeStyle.LineColor.Color = Color.White;
            shape.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;

            //Add text to the shape
            shape.AppendTextFrame("Hello World!");

            //Set the font and fill style of the text
            TextRange textRange = shape.TextFrame.TextRange;
            textRange.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            textRange.Fill.SolidColor.Color = System.Drawing.Color.CadetBlue;
            textRange.FontHeight = 66;
            textRange.LatinFont = new TextFont("Lucida Sans Unicode");

            //Save the document
            presentation.SaveToFile("HelloSpire.pptx", FileFormat.Pptx2010);

            //Save the PPT do PDF file format
            presentation.SaveToFile("HelloSpire.pdf", FileFormat.PDF);

        }


    }
}
